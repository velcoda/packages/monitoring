<?php

namespace Velcoda\Monitoring;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;
use Inspector\Laravel\Facades\Inspector;
use Inspector\Laravel\InspectorServiceProvider;
use Velcoda\Monitoring\Jobs\Heartbeat;

class MonitoringServiceProvider extends InspectorServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot()
    {
        $config = [
            'ignore_jobs' => [
                \Velcoda\Monitoring\Jobs\Heartbeat::class
            ],
            'http_client_body' => true
        ];

        config(['inspector.ignore_jobs' => $config['ignore_jobs']]);
        config(['inspector.http_client_body' => $config['http_client_body']]);

        Inspector::beforeFlush(function ($inspector) {
            $inspector->currentTransaction()
                ->host
                // You can get the desired service_name by config, env, etc.
                ->hostname = env('APP_NAME') . ' – ' . env('MODE');
        });
        parent::boot();
    }
}
