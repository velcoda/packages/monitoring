<?php

namespace Velcoda\Monitoring;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Velcoda\ApiAuth\Http\Middleware\AuthenticateApiRequests;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Velcoda\ApiAuth\Http\Middleware\AuthorizeApiKey;
use Velcoda\ApiAuth\Models\ApiKey;

class LoggingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $this->mergeConfigFrom(__DIR__.'/config/logging_channels.php', 'logging.channels');
    }
}
