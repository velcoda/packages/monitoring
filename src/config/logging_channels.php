<?php
return [
    'velcoda_stack' => [
        'driver' => 'stack',
        'channels' => ['daily', 'loki'],
        'ignore_exceptions' => false,
    ],
    'loki' => [
        'driver'         => 'custom',
        'level'          => env('LOG_LEVEL', 'debug'),
        'via'       => \Velcoda\Monitoring\Logging\VelcodaLokiHandler::class,
    ],
    'daily' => [
        'driver' => 'daily',
        'path' => storage_path('logs/laravel.log'),
        'level' => env('LOG_LEVEL', 'debug'),
        'days' => 3,
    ],
];
