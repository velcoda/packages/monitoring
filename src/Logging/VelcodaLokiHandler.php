<?php

namespace Velcoda\Monitoring\Logging;

use Itspire\MonologLoki\Formatter\LokiFormatter;
use Itspire\MonologLoki\Handler\LokiHandler;
use Monolog\Handler\WhatFailureGroupHandler;
use Monolog\Logger;

class VelcodaLokiHandler
{
    public function __invoke(array $config)
    {
        $handler = new LokiHandler(
            [
                'entrypoint' => env('LOKI_HOSTNAME'),
                'context' => [
                    // Set here your globally applicable context variables
                ],
                'labels' => [
                    // Set here your globally applicable labels
                ],
                'client_name' => env('APP_ENV') . '-' . env('APP_NAME'), // Here set a unique identifier for the client host
                // Optional : if you're using basic auth to authentify
                'auth' => [
                    'basic' => [
                        env('LOKI_HTTP_BASIC_USERNAME'), env('LOKI_HTTP_BASIC_PASSWORD')
                    ],
                ],
                // Optional : Override the default curl options with custom values
                'curl_options' => [
                    CURLOPT_CONNECTTIMEOUT_MS => 500,
                    CURLOPT_TIMEOUT_MS => 600
                ]
            ]
        );
        $formatter = new LokiFormatter([
            'labels' => [
                'environment' => env('APP_ENV'),
                'service' => env('APP_NAME'),
            ],
            'context' => [
                'service' => env('APP_NAME'),
                'context' => 'service',
                'company' => 'velcoda',
            ],
            'systemName' => env('LOKI_SYSTEM_NAME', null),
            'extraPrefix' => env('LOKI_EXTRA_PREFIX', ''),
            'contextPrefix' => env('LOKI_CONTEXT_PREFIX', '')
        ]);

        return new Logger('loki', [
            new WhatFailureGroupHandler([
                $handler->setFormatter($formatter)
            ])
        ]);
    }
}
