<?php

namespace Velcoda\Monitoring;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Velcoda\Monitoring\Jobs\Heartbeat;

class SchedulerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->booted(function () {
            if (env('APP_ENV') === "production") {
                $schedule = $this->app->make(Schedule::class);
                if (!!env('HEARTBEAT_URI')) {
                    $schedule->job(new Heartbeat())->everyMinute();
                }
            }
        });
    }

    public function register()
    {
    }
}
